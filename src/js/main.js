import { SectionCreator } from './components/SectionCreator.js';
import '../styles/style.css';
import './components/WebsiteSection';
import { subscribeToProgramButtons, unsubscribeFromProgramButtons } from './utils/join-us-section';
import { JOIN_PROGRAM_SECTION_BUTTON, JOIN_PROGRAM_SECTION_EMAIL } from './constants/selectors-constants';
import { collectMetrics } from './performance';

const observer = new PerformanceObserver(collectMetrics);
observer.observe({ entryTypes: ['navigation', 'resource'] }, observer);

window.addEventListener('DOMContentLoaded', () => {
  SectionCreator.create('community').render();
  SectionCreator.create('standard').render();
  const worker = new Worker(new URL('./worker.js', import.meta.url));
  const clickableElements = document.querySelectorAll('button, input');

  clickableElements.forEach(element => {
    element.addEventListener('click', event => {
      worker.postMessage(event.target.nodeName);
    });
  });

  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register(new URL('./serviceWorker.js', import.meta.url))
        .then(() => console.log('Service worker registered successfully.'))
        .catch(() => console.log('Service worker failed to register.'));

    navigator.serviceWorker.addEventListener('message', event => {
      const button = document.getElementsByClassName(JOIN_PROGRAM_SECTION_BUTTON)[0];
      const input = document.getElementsByClassName(JOIN_PROGRAM_SECTION_EMAIL)[0];
      const { action } = event.data;
      switch (action) {
        case 'subscribeFailed':
          unsubscribeFromProgramButtons(button, input);
          break;
        case 'unsubscribeFailed':
          subscribeToProgramButtons(button, input);
          break;
        default:
          console.log('Unknown action from Service Worker.');
      }
    });
  } else {
    console.log('Service workers are not supported.');
  }
});
