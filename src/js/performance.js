import { postData } from './utils/fetch-data-utils';

const metrics = {};
const metricsToSend = ['communityLoadTime', 'pageLoadTime', 'memoryUsage'];

export function collectMetrics(list, observer) {
  const entries = list.getEntries();
  entries.forEach(entry => {
    if (entry.name.includes('api/community')) {
      metrics.communityLoadTime = entry.duration;
    } else if (entry.name.endsWith('/')) {
      metrics.pageLoadTime = entry.duration;
    }
  });
  metrics.memoryUsage = performance.memory.usedJSHeapSize;

  if (metricsToSend.every(metric => metric in metrics)) {
    sendMetrics();
    observer.disconnect();
  }
}

export function sendMetrics() {
  postData('api/analytics/performance', metrics)
      .then(() => console.log('Performance data sent.'))
      .catch(() => console.log('Unable to send performance data.'));
}
