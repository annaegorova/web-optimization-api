import { makeRequest } from '../utils';
const clickEvents = [];

self.addEventListener('message', event => {
  clickEvents.push(event.data);
  if (clickEvents.length >= 5) {
    makeRequest('api/analytics/user', 'POST', clickEvents.slice(0, 5))
        .then(() => {
          clickEvents.splice(0, 5);
          postMessage(clickEvents);
        })
        .catch(error => {
          throw new Error(error);
        });
  }
});
