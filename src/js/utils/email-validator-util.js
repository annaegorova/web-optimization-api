const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

export function validate(email) {
  return VALID_EMAIL_ENDINGS.some(ending => email.endsWith(ending));
}

export function validateAsync(email) {
  return Promise.resolve(validate(email));
}

export function validateWithThrow(email) {
  if (validate(email)) return true;
  throw new Error(`Provided email ${email} is invalid`);
}

export function validateWithLog(email) {
  const emailValid = validate(email);
  console.log(emailValid);
  return emailValid;
}
