import { validate } from './email-validator-util';
import { addNewElement, createElement } from './create-elements-utils';
import {
  JOIN_PROGRAM_BUTTON_SUBSCRIBE,
  JOIN_PROGRAM_BUTTON_UNSUBSCRIBE,
  JOIN_PROGRAM_EMAIL_PLACEHOLDER,
  JOIN_SECTION_DESCRIPTION
} from '../constants/content-constants';
import {
  APP_CONTAINER,
  APP_FOOTER,
  APP_SECTION,
  APP_SECTION_BUTTON,
  APP_SUBTITLE,
  APP_TITLE,
  HIDDEN,
  JOIN_PROGRAM_SECTION,
  JOIN_PROGRAM_SECTION_BUTTON,
  JOIN_PROGRAM_SECTION_EMAIL,
  JOIN_PROGRAM_SECTION_FORM
} from '../constants/selectors-constants';
import { postData } from './fetch-data-utils';

export function addJoinUsSection(headerText, submitButtonText) {
  const section = createElement('section', [APP_SECTION, JOIN_PROGRAM_SECTION]);

  addNewElement(section, 'h2', APP_TITLE, headerText);
  addNewElement(section, 'h3', APP_SUBTITLE, JOIN_SECTION_DESCRIPTION);
  section.appendChild(createJoinUsSectionForm(submitButtonText));

  appendJoinUsSection(section);
}

function appendJoinUsSection(section) {
  const app = document.getElementById(APP_CONTAINER);
  const footer = document.getElementsByClassName(APP_FOOTER)[0];
  app.insertBefore(section, footer);
}

function createJoinUsSectionForm(submitButtonText) {
  const form = createElement('form', JOIN_PROGRAM_SECTION_FORM);

  const userSubscribed = isSubscribedToProgram();
  const input = createJoinUsFormInput(userSubscribed);
  const button = createJoinUsFormSubmitButton(userSubscribed, submitButtonText);

  form.appendChild(input);
  form.appendChild(button);
  form.addEventListener('submit', async event => {
    event.preventDefault();
    if (isSubscribedToProgram()) await unsubscribeFromProgram(button, input);
    else await validateAndSubscribeToProgram(button, input);
  });

  return form;
}

function createJoinUsFormSubmitButton(userSubscribed, submitButtonText) {
  const buttonText = userSubscribed ?
    JOIN_PROGRAM_BUTTON_UNSUBSCRIBE :
    submitButtonText;
  const button = createElement(
      'button',
      [APP_SECTION_BUTTON, JOIN_PROGRAM_SECTION_BUTTON],
      buttonText
  );

  return button;
}

function createJoinUsFormInput(userSubscribed) {
  const inputClasses = userSubscribed ?
    [JOIN_PROGRAM_SECTION_EMAIL, HIDDEN] :
    JOIN_PROGRAM_SECTION_EMAIL;
  const input = createElement('input', inputClasses);

  input.setAttribute('type', 'text');
  input.setAttribute('placeholder', JOIN_PROGRAM_EMAIL_PLACEHOLDER);
  input.value = getEmailFromStorage();
  input.addEventListener('input', e => {
    addEmailToStorage(e.target.value);
  });

  return input;
}

function disableButton(button) {
  button.setAttribute('disabled', 'true');
  button.classList.add('disabled');
}

function enableButton(button) {
  button.removeAttribute('disabled');
  button.classList.remove('disabled');
}

function isSubscribedToProgram() {
  return localStorage.getItem('subscribedToProgram') === 'true';
}

function toggleSubscription() {
  localStorage.setItem('subscribedToProgram', !isSubscribedToProgram());
}

function getEmailFromStorage() {
  return localStorage.getItem('subscriptionEmail');
}

function addEmailToStorage(email) {
  localStorage.setItem('subscriptionEmail', email);
}

function removeEmailFromStorage() {
  localStorage.removeItem('subscriptionEmail');
}

async function validateAndSubscribeToProgram(button, input) {
  if (validate(input.value)) {
    if (navigator.online) {
      disableButton(button);
      try {
        await postData('/api/subscribe', {
          email: input.value
        });
        subscribeToProgramButtons(button, input);
      } catch (error) {
        if (error.status === 422) {
          alert(`${error.status}: ${error.message}`);
        }
        enableButton(button);
      }
    } else if ('SyncManager' in window) {
      try {
        const registration = await navigator.serviceWorker.getRegistration();
        const registrationData = JSON.stringify({ action: 'subscribe', payload: input.value });
        await registration.sync.register(registrationData);
        subscribeToProgramButtons(button, input);
      } catch (error) {
        console.log(error);
      }
    }
  }
}

async function unsubscribeFromProgram(button, input) {
  disableButton(button);
  if (navigator.onLine) {
    try {
      await postData('/api/unsubscribe', {
        email: input.value
      });
      unsubscribeFromProgramButtons(button, input);
    } catch (error) {
      console.log(`${error.status}: ${error.message}`); // for now let's console
      enableButton(button);
    }
  } else if ('SyncManager' in window) {
    try {
      const registration = await navigator.serviceWorker.getRegistration();
      const registrationData = JSON.stringify({ action: 'unsubscribe', payload: input.value });
      await registration.sync.register(registrationData);
      unsubscribeFromProgramButtons(button, input);
    } catch (error) {
      console.log(error);
    }
  }
}

export function subscribeToProgramButtons(button, input) {
  toggleSubscription();
  enableButton(button);
  removeEmailFromStorage();
  button.textContent = JOIN_PROGRAM_BUTTON_UNSUBSCRIBE;
  input.classList.add(HIDDEN);
}

export function unsubscribeFromProgramButtons(button, input) {
  toggleSubscription();
  removeEmailFromStorage();
  enableButton(button);
  button.textContent = JOIN_PROGRAM_BUTTON_SUBSCRIBE;
  input.value = '';
  input.classList.remove(HIDDEN);
}
